import java.util.Scanner;

public class Schleifen_1_Aufgabe_6 {

	public static void main(String[] args) {
		final Scanner eingabe = new Scanner(System.in);
		
		System.out.print("Geben sie die gewŁnschte Anzahl von Sternen ein: ");
		int anzahl = eingabe.nextInt();
		
		System.out.print("\n");
		
		for (int i = 1; i <= anzahl; i++) {
			
			for (int x = 1; x <= i; x++) {
				System.out.print("*");
			}
			
			System.out.print("\n");
			
		}
		
		eingabe.close();
	}

}
