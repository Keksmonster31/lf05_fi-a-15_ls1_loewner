import java.util.Scanner;

public class Schleifen_2_Aufgabe_4 {

	public static void main(String[] args) {
		Scanner eingabe = new Scanner(System.in);
		double startWert;
		double endWert;
		double schrittweite;
		
		System.out.print("Bitte den Startwert in Celsius eingeben: ");
		startWert = eingabe.nextDouble();
		
		System.out.print("Bitte den Endwert in Celsius eingeben: ");
		endWert = eingabe.nextDouble();
		
//		while (endWert < startWert) {
//			System.out.print("Ung�ltige Eingabe, Endwert ist kleiner als Startwert!\n"
//							+"Bitte einen neuen Endwert angeben: ");
//			endWert = eingabe.nextDouble();
//		}
		
		System.out.print("Bitte die Schrittweite in Grad Celsius eingeben: ");
		schrittweite = eingabe.nextDouble();
		
		do {
			System.out.printf("%5.2f�C = %5.2f�F\n", startWert, startWert * 1.8 + 32);
			startWert += schrittweite;
		} while (startWert < endWert);
		
		eingabe.close();
	}

}
