import java.util.Scanner;

public class Schleifen_1_Aufgabe_1 {

	public static void main(String[] args) {
		Scanner eingabe = new Scanner(System.in);
		int zahl;
		int richtung;
		
		System.out.print("Bitte geben sie eine Zahl ein: ");
		zahl = eingabe.nextInt();
		
		System.out.println("\nM�chten Sie (h)och oder (r)unter z�hlen?");
		richtung = eingabe.next().toLowerCase().charAt(0);
		
		if (richtung == 'h') {
			for (int i = 1; i <= zahl; i++) {
				System.out.println(i);
			}
		} else if (richtung == 'r') {
			for (int i = zahl; i >= 1; i--) {
				System.out.println(i);
			}
		}
		
		eingabe.close();
	}

}
