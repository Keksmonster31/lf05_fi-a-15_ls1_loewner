import java.util.Scanner;

public class Schleifen_2_Aufgabe_2 {

	public static void main(String[] args) {
		Scanner eingabe = new Scanner(System.in);
		int zahl;
		
		System.out.print("Bitte geben sie eine Zahl zwischen 0 und 20 ein,\n"
						+"von der die Fakult�t errechnet werden soll: ");
		zahl = eingabe.nextInt();
		
		while (zahl < 0 || zahl > 20) {
			System.out.print("\nUng�ltige Eingabe!\n"
							+"Bitte eine Zahl zwischen 0 und 20 eingeben: ");
			zahl = eingabe.nextInt();
		}
		
		if (zahl == 0)
			System.out.println("\nDie Fakult�t von 0 ist 1.");
		else {
			int i = zahl - (zahl - 1);
			int ergebnis = i;
			while (i <= zahl) {
				ergebnis *= i;
				i++;
			}
			System.out.print("\nDie Fakult�t von " + zahl + " ist " + ergebnis + ".");
		}
		
		eingabe.close();		
	}

}
