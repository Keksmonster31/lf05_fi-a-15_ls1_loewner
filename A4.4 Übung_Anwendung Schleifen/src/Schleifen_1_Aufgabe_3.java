
public class Schleifen_1_Aufgabe_3 {

	public static void main(String[] args) {
		
		for (int i = 1; i <= 200; i++) {
			
			System.out.println("Aktuelle Zahl: " + i);
			
			if (i % 7 == 0) {
				System.out.println(i + " ist durch 7 glatt teilbar.");
			}
			
			if (i % 5 != 0 && i % 4 == 0) {
				System.out.println(i + " ist durch 4 glatt teilbar aber nicht durch 5.");
			}
			
		}

	}

}
