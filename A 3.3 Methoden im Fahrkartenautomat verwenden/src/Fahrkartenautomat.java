﻿import java.util.Scanner;

class Fahrkartenautomat
{
	public static Scanner tastatur = new Scanner(System.in);
	
    public static void main(String[] args)
    {
    	double zwischenspeicher;
    	
    	zwischenspeicher = fahrkartenbestellungErfassen();
    	
    	zwischenspeicher = fahrkartenBezahlen(zwischenspeicher);
    	
    	fahrkartenAusgeben();
    	
    	rueckgeldAusgeben(zwischenspeicher);
    }
    
    public static double fahrkartenbestellungErfassen() {
    	
    	double 	zuZahlenderBetrag;
        byte		anzahlTickets;

        // Preis pro Ticket statt Gesamtpreis
        System.out.print("Preis pro Ticket (EURO): ");
        zuZahlenderBetrag = tastatur.nextDouble();
        
        // Nachfrage zu Eingabe der Zahl der gewünschten Tickets
        System.out.print("Anzahl der Tickets: ");
        anzahlTickets = tastatur.nextByte();
        
        // Check ob eine Zahl von mindestens einem Ticket eingegeben wurde.
        // 0 und negative Zahlen sind ungültig und fragen nach erneuter Eingabe
        while (anzahlTickets < 1) {
     	   System.out.println("Ungültige Zahl an Tickets");
     	   System.out.print("Bitte geben Sie eine Zahl größer als 0 ein: ");
     	   anzahlTickets = tastatur.nextByte();
        }
        
        // Der zu Zahlende Betrag wird mit der Zahl der Tickets multipliziert 
        return zuZahlenderBetrag * (double) anzahlTickets;
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	
    	double eingezahlterGesamtbetrag;
    	double eingeworfeneMünze;
    	
    	// Geldeinwurf
        // -----------
        eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("%s%.2f%s\n", "Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag), " Euro");
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
    	
        return eingezahlterGesamtbetrag - zuZahlenderBetrag; 

    }
    
    public static void fahrkartenAusgeben() {

    	// Fahrscheinausgabe
	    // -----------------
	    // Wenn mehr als ein Fahrschein gekauft wurde ändert sich der Text
	 	System.out.println("\n Ihre Fahrscheine werden ausgegeben");
	
	    for (int i = 0; i < 8; i++)
	    {
	       System.out.print("=");
	       try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	    System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double rückgabebetrag) {

        if(rückgabebetrag > 0.0)
        {
     	   System.out.printf("%s%.2f%s\n","Der Rückgabebetrag in Höhe von ", rückgabebetrag, " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht Ihre Fahrscheine\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.");
    	
    }
    
}
