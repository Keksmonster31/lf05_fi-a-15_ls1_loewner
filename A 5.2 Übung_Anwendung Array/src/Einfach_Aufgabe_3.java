import java.util.Scanner;

public class Einfach_Aufgabe_3 {

	public static void main(String[] args) {
		Scanner eingabe 	= new Scanner(System.in); 
		char[] 	palindrom	= new char[5]; 
		
		for(int i = 0; i < palindrom.length; i++) {
			System.out.print("Bitte geben Sie den "+ (i + 1) +". Buchstaben ein: ");
			palindrom[i] = eingabe.next().charAt(0);
		}
		
		int x = (palindrom.length - 1);
		System.out.print("\nUmgekehrt ergibt Ihr Wort: ");
		for(int i = 0; i < palindrom.length; i++) {
			System.out.print(palindrom[x]);
			x--;
		}
		
		eingabe.close();
	}

}
