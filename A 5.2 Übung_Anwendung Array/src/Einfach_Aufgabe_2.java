
public class Einfach_Aufgabe_2 {

	public static void main(String[] args) {
		
		int[] 	zahlenArray = new int[10];
		int		ungerade	= 1;
		
		for(int i = 0; i < zahlenArray.length; i++) {
			zahlenArray[i] = ungerade;
			ungerade += 2;
		}
		
		for(int i = 0; i < zahlenArray.length; i++) {
			System.out.println(zahlenArray[i]);
		}

	}

}
