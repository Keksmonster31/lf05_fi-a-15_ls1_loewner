import java.util.Scanner;

public class Aufgabe_2 {

	public static void main(String[] args) {
		
		Scanner userInput = new Scanner(System.in);
		
		System.out.println("Hallo werter Nutzer.\n");
		
		System.out.print("Bitte geben Sie Ihren Namen ein: ");
		String name = userInput.next();
		System.out.print("Bitte geben sie Ihr Alter an: ");
		int age = userInput.nextInt();
		
		System.out.println("\n\nIhr Name lautet " + name + " und Sie sind " + age + " Jahre alt.");
		System.out.println("Einen sch�nen Tag noch :)");
		
		userInput.close();
	}

}
