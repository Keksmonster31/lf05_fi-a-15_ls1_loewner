import java.util.Scanner;

public class JavaTrain {

	public static void main(String[] args) {
		int fahrzeit = 0;
		char haltInSpandau = 'n';
		char richtungHamburg = 'n';
		char haltInStendal = 'j';
		char endetIn = 'h';

		fahrzeit = fahrzeit + 8; // Fahrzeit: Berlin Hbf -> Spandau

		if (haltInSpandau == 'j')
			fahrzeit = fahrzeit + 2; // Halt in Spandau

		if (richtungHamburg == 'j') {
			fahrzeit = fahrzeit + 96; // Fahrzeit: Spandau -> Hamburg
			System.out.print("Sie erreichen Hamburg nach ");
		} else {

			fahrzeit = fahrzeit + 34; // Fahrzeit: Spandau -> Haltepunkt �stlich von Stendal

			if (haltInStendal == 'j') {
				fahrzeit = fahrzeit + 16; // �ber Stendal Hbf
			} else {
				fahrzeit = fahrzeit + 6; // Umfahren von Stendal
			}

			if (endetIn == 'h') {
				fahrzeit = fahrzeit + 62; // Fahrzeit: Haltepunkt westlich von Stendal -> Hannover
				System.out.print("Sie erreichen Hannover nach ");
			} else if (endetIn == 'b') {
				fahrzeit = fahrzeit + 50; // Fahrzeit: Haltepunkt westlich von Stendal -> Braunschweig
				System.out.print("Sie erreichen Braunschweig nach ");
			} else {
				fahrzeit = fahrzeit + 29; // Fahrzeit: Haltepunkt westlich von Stendal -> Wolfsburg
				System.out.print("Sie erreichen Wolfsburg nach ");
			}

		}

		System.out.printf("%d Minuten", fahrzeit);
	}

}
