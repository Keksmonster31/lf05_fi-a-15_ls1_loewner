
public class Aufgabe2 {
	
	public static void main(String[] args) {
		
		// Aufgabe 2
		System.out.printf("%15s", "*");
		System.out.printf("%n%16s", "***");
		System.out.printf("%n%17s", "*****");
		System.out.printf("%n%18s", "*******");
		System.out.printf("%n%19s", "*********");
		System.out.printf("%n%20s", "***********");
		System.out.printf("%n%21s", "*************");
		System.out.printf("%n%16s", "***");
		System.out.printf("%n%16s%n%n", "***");
	}

}