
public class RaumschiffTest {

	public static void main(String[] args) {
		
		Raumschiff klingonen = new Raumschiff(	1,
												100,
												100,
												100,
												100,
												2,
												"IKS Hegh'ta");
		
		Raumschiff romulaner = new Raumschiff(	2,
												100,
												100,
												100,
												100,
												2,
												"IRW Khazara");
		
		Raumschiff vulkanier = new Raumschiff(	0,
												80,
												80,
												50,
												100,
												5,
												"Ni'Var");
		
		System.out.print(	"Schiffsname: "				+ klingonen.getSchiffsname() + "\n" +
							"Photonentorpedos: "		+ klingonen.getPhotonentorpedoAnzahl() + "\n" +
							"Energieversorgung: "		+ klingonen.getEnergieversorgungInProzent() + "\n" +
							"Schilde: "					+ klingonen.getSchildeInProzent() + "\n" +
							"H�lle: "					+ klingonen.getHuelleInProzent() + "\n" +
							"Lebenserhaltungssysteme: "	+ klingonen.getLebenserhaltungssystemeInProzent() + "\n" +
							"Anzahl Droiden: "			+ klingonen.getAndroidenAnzahl() + "\n\n");
		
		System.out.print(	"Schiffsname: "				+ romulaner.getSchiffsname() + "\n" +
							"Photonentorpedos: "		+ romulaner.getPhotonentorpedoAnzahl() + "\n" +
							"Energieversorgung: "		+ romulaner.getEnergieversorgungInProzent() + "\n" +
							"Schilde: "					+ romulaner.getSchildeInProzent() + "\n" +
							"H�lle: "					+ romulaner.getHuelleInProzent() + "\n" +
							"Lebenserhaltungssysteme: "	+ romulaner.getLebenserhaltungssystemeInProzent() + "\n" +
							"Anzahl Droiden: "			+ romulaner.getAndroidenAnzahl() + "\n\n");
		
		System.out.print(	"Schiffsname: "				+ vulkanier.getSchiffsname() + "\n" +
							"Photonentorpedos: "		+ vulkanier.getPhotonentorpedoAnzahl() + "\n" +
							"Energieversorgung: "		+ vulkanier.getEnergieversorgungInProzent() + "\n" +
							"Schilde: "					+ vulkanier.getSchildeInProzent() + "\n" +
							"H�lle: "					+ vulkanier.getHuelleInProzent() + "\n" +
							"Lebenserhaltungssysteme: "	+ vulkanier.getLebenserhaltungssystemeInProzent() + "\n" +
							"Anzahl Droiden: "			+ vulkanier.getAndroidenAnzahl() + "\n\n");
	}

}
