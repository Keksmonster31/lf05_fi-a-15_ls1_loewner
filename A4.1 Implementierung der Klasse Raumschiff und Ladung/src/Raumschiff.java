import java.util.ArrayList;

public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator;
	private ArrayList<Ladung> ladungsverzeichnis;

	// Konstruktoren
	public Raumschiff() {}
	
	public Raumschiff(	int photonentorpedoAnzahl,
						int energieversorgungInProzent,
						int zustandSchildeInProzent,
						int zustandHuelleInProzent,
						int zustandLebenserhaltungssystemeInProzent,
						int anzahlDroiden,
						String schiffsname)
	{
		this.setPhotonentorpedoAnzahl(photonentorpedoAnzahl);
		this.setEnergieversorgungInProzent(energieversorgungInProzent);
		this.setSchildeInProzent(zustandSchildeInProzent);
		this.setHuelleInProzent(zustandHuelleInProzent);
		this.setLebenserhaltungssystemeInProzent(zustandLebenserhaltungssystemeInProzent);
		this.setAndroidenAnzahl(anzahlDroiden);
		this.setSchiffsname(schiffsname);
		
	}
	
	// PhotonenTorpedoAnzahl
	public int getPhotonentorpedoAnzahl() {
		return this.photonentorpedoAnzahl;
	}
	
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		if (photonentorpedoAnzahl < 0)
			this.photonentorpedoAnzahl = 0;
		else if(photonentorpedoAnzahl > 100)
			this.photonentorpedoAnzahl = 100;
		else
			this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}
	
	// EnergieversorgungInProzent
	public int getEnergieversorgungInProzent() {
		return this.energieversorgungInProzent;
	}
	
	public void setEnergieversorgungInProzent (int energieversorgungInProzent) {
		if (energieversorgungInProzent < 0)
			this.energieversorgungInProzent = 0;
		else if(energieversorgungInProzent > 100)
			this.energieversorgungInProzent = 100;
		else
			this.energieversorgungInProzent = energieversorgungInProzent;
	}
	
	// SchileInProzent
	public int getSchildeInProzent() {
		return this.schildeInProzent;
	}
	
	public void setSchildeInProzent(int schildeInProzent) {
		if (schildeInProzent < 0)
			this.schildeInProzent = 0;
		else if(schildeInProzent > 100)
			this.schildeInProzent = 100;
		else
			this.schildeInProzent = schildeInProzent;
	}
	
	// HuelleInProzent
	public int getHuelleInProzent() {
		return this.huelleInProzent;
	}
	
	public void setHuelleInProzent(int huelleInProzent) {
		if (huelleInProzent < 0)
			this.huelleInProzent = 0;
		else if(huelleInProzent > 100)
			this.huelleInProzent = 100;
		else
			this.huelleInProzent = huelleInProzent;
	}
	
	// LebenserhaltungssystemeInProzent
	public int getLebenserhaltungssystemeInProzent() {
		return this.lebenserhaltungssystemeInProzent;
	}
	
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		if (lebenserhaltungssystemeInProzent < 0)
			this.lebenserhaltungssystemeInProzent = 0;
		else if(lebenserhaltungssystemeInProzent > 100)
			this.lebenserhaltungssystemeInProzent = 100;
		else
			this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}
	
	// AndroidenAnzahl
	public int getAndroidenAnzahl() {
		return this.androidenAnzahl;
	}
	
	public void setAndroidenAnzahl(int androidenAnzahl) {
		if (androidenAnzahl < 0)
			this.androidenAnzahl = 0;
		else
			this.androidenAnzahl = androidenAnzahl;
	}
	
	// Schiffsname
	public String getSchiffsname() {
		return this.schiffsname;
	}
	
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	
	// NeueLadung
	public void addLadung(Ladung neueLadung) {
		this.ladungsverzeichnis.add(neueLadung);
	}
	
	// PhotonenTorpedoSchiessen
	public void photonenTorpedoSchiessen(Raumschiff r) {
//		if(this.photonenTorpedoAnzahl < 1)
//			return;
//		else
//			setPhotonenTorpedoAnzahl(getPhotonenTorpedoAnzahl() - 1);
	}
	
	// PhaserkanoneSchiessen
	public void phaserkanoneSchiessen(Raumschiff r) {
		
	}
	
	// Treffer
	private void treffer(Raumschiff r) {
		
	}
	
	// NachrichtAnAlle
	public void nachrichtAnAlle(String message) {
		
	}
	
	// EintraegeLogbuchZurueckgeben
	public ArrayList<String> eintraegeLogbuchZurueckgeben() {
		
		return this.broadcastKommunikator;
	}
	
	// PhotonentorpedosLaden
	public void photonentorpedosLaden(int anzahlTorpedos) {
		
	}
	
	// ReparaturDurchfuehren
	public void reparaturDurchfuehren(	boolean schutzschilde,
										boolean energieversorgung,
										boolean schiffshuelle,
										int anzahlDroiden)
	{
		
	}
	
	// ZustandRaumschiff
	public void zustandRaumschiff() {
		
	}
	
	// LadungsverzeichnisAusgeben
	public void ladungsverzeichnisAusgeben() {
		
	}
	
	// LadungsverzeichnisAufraeumen
	public void ladungsverzeichnisAufraeumen() {
		
	}
}
