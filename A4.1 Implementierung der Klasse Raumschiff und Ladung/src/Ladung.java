
public class Ladung {
	
	private String bezeichnung;
	private int menge;
	
	// Konstruktoren
	public Ladung() {}
	
	public Ladung(String bezeichnung, int menge) {
		setBezeichnung(bezeichnung);
		setMenge(menge);
	}

	// Bezeichnung
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung; 
	}
	
	public String getBezeichnung() {
		return this.bezeichnung;
	}
	
	// Menge
	public void setMenge(int menge) {
		if(menge < 0)
			this.menge = 0;
		else
			this.menge = menge;
	}
	
	public int getMenge() {
		return this.menge;
	}
	
	// Bezeichnung + Menge zu String
	public String toString() {
		return getBezeichnung() + getMenge();
	}
}
