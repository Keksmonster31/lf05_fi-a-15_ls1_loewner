import java.util.Scanner;

class Fahrkartenautomat {

	/*
	 * Aufgabe 1: Welche Vorteile hat man durch diesen Schritt?
	 * Durch die Nutzung von Arrays muss man nicht f�r jede Auswahl einen Switch-case oder eine if-Bedingung einf�gen.
	 * Man kann einfach den zwei Arrays jeweils einen Eintrag hinzuf�gen um eine Fahrkarte hinzuzuf�gen.
	 * Das Verschieben und L�schen von Eintr�gen ist auch leichter.
	 */
	
	/*
	 * Aufgabe 3: Vergleichen Sie die neue Implementierung mit der alten und erl�utern Sie die Vor- und Nachteile der jeweiligen Implementierung.
	 * Alte Implementierung
	 * 	- Vorteile
	 * 		
	 * 	- Nachteile
	 * 		F�r jeden Eintrag muss ein switch-case/if-else statement eingef�gt werden. Mehr Schreibaufwand.
	 * Neue Implementierung
	 * 	- Vorteile
	 * 		Eintr�ge lassen sich leicht hinzuf�gen/l�schen/�ndern
	 * 	- Nachteile
	 * 		Wenn die Reihenfolge der "tickets" und "TicketPreise" nicht gleich sind, wird falsch gerechnet.
	 */
	
    public static double fahrkartenbestellungErfassen() {
    	String[] tickets 		= {	"Einzelfahrschein Berlin AB", 
		    						"Einzelfahrschein Berlin BC",
		    						"Einzelfahrschein Berlin ABC",
		    						"Kurzstrecke",
		    						"24-Stunden-Karte Berlin AB",
		    						"24-Stunden-Karte Berlin BC",
		    						"24-Stunden-Karte Berlin ABC", 
		    						"24-Stunden-Karte Kleingruppe Berlin AB", 
		    						"24-Stunden-Karte Kleingruppen Berlin BC", 
		    						"24-Stunden-Karte Kleingruppen Berlin ABC"
		};
    	double[] ticketPreise 	= {	3.00,
    								3.50,
    								3.80,
    								2.00,
    								8.80,
    								9.20,
    								10.00,
    								25.50,
    								26.00,
    								26.50
    	};
        int ticketWahl;
    	int anzahlTickets;
        double gesamtPreis;
        Scanner tastatur = new Scanner(System.in);
        
        do {
	        System.out.println("W�hlen Sie Ihre gew�nschte Fahrkarte aus:");
	        System.out.printf("%-4s%-50s %-4s\n", "Nr.", "Bezeichnung", "Preis in Euro");
	        for (int i = 0; i < tickets.length; i++) {
	        	// Auf "i" wird "+ 1" gerechnet damit die Liste bei 1 beginnt, logischer f�r Benutzer.
	        	System.out.printf("%-4d%-50s: %.2f\n" , i + 1, tickets[i], ticketPreise[i]);
	        }
	        System.out.print("\nIhre Wahl: ");
	        
	        // damit die Wahl wieder dem Index im Array entspricht
	        ticketWahl = tastatur.nextInt() - 1;
	        if (ticketWahl < 0 || ticketWahl > tickets.length) {
	        	System.out.println("Ung�ltige Eingabe!\n");
	        	continue;
	        } else {
	        	break;
	        }
        } while (true);
        
        gesamtPreis = ticketPreise[ticketWahl];

        System.out.print("Anzahl der Tickets (min: 1 | max: 10): ");
        anzahlTickets = tastatur.nextInt();
        
        if (anzahlTickets < 1 || anzahlTickets > 10) {
        	System.out.printf("Die Eingabe f�r die Anzahl der Tickets ist ung�ltig (%d).\nDer Wert muss zwischen 1-10 liegen. \nWert wird auf \"1\" gesetzt.\n", anzahlTickets);
        	anzahlTickets = 1;
        }

        return gesamtPreis * anzahlTickets;
    }

    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
        double eingeworfenemuenze;
        double eingezahlterGesamtbetrag = 0.0;
        Scanner tastatur = new Scanner(System.in);

        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.format("\nNoch zu zahlen: %4.2f Euro %n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
            eingeworfenemuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfenemuenze;
        }
        return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }

    public static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }

    public static void rueckgeldAusgeben(double rueckgabebetrag) {

        if (rueckgabebetrag > 0.0) {
            System.out.format("Der R�ckgabebetrag in H�he von %4.2f Euro %n", rueckgabebetrag);
            System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while (rueckgabebetrag >= 2.0) {// 2 EURO-muenzen
                System.out.println("2 EURO");
                rueckgabebetrag -= 2.0;
            }
            while (rueckgabebetrag >= 1.0) {// 1 EURO-muenzen
                System.out.println("1 EURO");
                rueckgabebetrag -= 1.0;
            }
            while (rueckgabebetrag >= 0.5) // 50 CENT-muenzen
            {
                System.out.println("50 CENT");
                rueckgabebetrag -= 0.5;
            }
            while (rueckgabebetrag >= 0.2) // 20 CENT-muenzen
            {
                System.out.println("20 CENT");
                rueckgabebetrag -= 0.2;
            }
            while (rueckgabebetrag >= 0.1) // 10 CENT-Müzen
            {
                System.out.println("10 CENT");
                rueckgabebetrag -= 0.1;
            }
            while (rueckgabebetrag >= 0.05)// 5 CENT-muenzen
            {
                System.out.println("5 CENT");
                rueckgabebetrag -= 0.05;
            }
        }
    }

    public static void main(String[] args) {
    	
    	while (true) {
	        double zuZahlenderBetrag;
	        double rueckgabebetrag;
	
	        zuZahlenderBetrag = fahrkartenbestellungErfassen();
	        rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
	        fahrkartenAusgeben();
	        rueckgeldAusgeben(rueckgabebetrag);
	
	        System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
	                + "Wir w�nschen Ihnen eine gute Fahrt.");
	        System.out.print("\n\n\n");
    	}
    }
}