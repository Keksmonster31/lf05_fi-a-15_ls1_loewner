import java.util.Scanner;

public class Aufgabe_3 {

	public static void main(String[] args) {
		Scanner eingabe = new Scanner(System.in);
		int anzahl;
		double einzelpreis;
		double endpreis;
		
		System.out.print("Bitte geben sie die Anzahl der bestellten M�use ein: ");
		anzahl = eingabe.nextInt();
		System.out.print("Bitte geben sie den Einzelpreis an: ");
		einzelpreis = eingabe.nextDouble();
		
		endpreis = einzelpreis * (double) anzahl;
		endpreis *= 1.19;
		
		if (anzahl >= 10) {
			System.out.println("Bestellte Anzahl ist mindestens 10.\n"
					+ "Kostenlose Lieferung.");
		} else {
			System.out.println("Bestellte Anzahl ist unter 10.\n"
					+ "Eine Lieferpauschale in H�he von 10 � wird erhoben.");
			endpreis += 10.0;
		}
		
		System.out.printf("\nDer Endpreis betr�gt %.2f �", endpreis);
		
		eingabe.close();
	}

}
