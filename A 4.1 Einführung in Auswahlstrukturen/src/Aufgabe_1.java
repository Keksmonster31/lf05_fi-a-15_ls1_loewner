import java.util.Scanner;

public class Aufgabe_1 {

	public static void main(String[] args) {
		
		Scanner eingabe = new Scanner(System.in);
		
		// 2 Zahlen	
		int zahl1;
		int zahl2;
		
		System.out.print("Bitte gebe eine erste (Ganz-)Zahl ein: ");
		zahl1 = eingabe.nextInt();
		
		System.out.print("Bitte gebe eine zweite (Ganz-)Zahl ein: ");
		zahl2 = eingabe.nextInt();
		
		// 1.
		// Wenn dann Aktivit�ten aus dem Alltag:
		// Wenn Schule dann konzentrieren
		// Wenn Hunger dann essen
		// Wenn Neujahr dann feiern
		
		// 2.
		if (zahl1 == zahl2)
			System.out.println("Beide Zahlen sind gleich gro�.");
		
		// 3.
		if (zahl1 < zahl2)
			System.out.printf("Zahl 2 (%d) ist gr��er als Zahl 1 (%d)\n", zahl2, zahl1);
		
		// 4.
		if (zahl1 > zahl2) 
			System.out.printf("Zahl 1 (%d) ist gr��er Zahl 2 (%d)\n", zahl1, zahl2);
		else if (zahl1 == zahl2)
			System.out.printf("Zahl 1 (%d) ist gleich als Zahl 2 (%d)\n", zahl1, zahl2);
		else
			System.out.printf("Zahl 1 (%d) ist NICHT gr��er oder gleich als Zahl 2\n", zahl1, zahl2);
		
		// 3 Zahlen
		System.out.print("\nBeginn des Programmteils mit 3 Eingaben\n\n");
		
		int zahl3;
		
		System.out.print("Bitte gebe eine erste (Ganz-)Zahl ein: ");
		zahl1 = eingabe.nextInt();
		
		System.out.print("Bitte gebe eine zweite (Ganz-)Zahl ein: ");
		zahl2 = eingabe.nextInt();
		
		System.out.print("Bitte gebe eine dritte (Ganz-)Zahl ein: ");
		zahl3 = eingabe.nextInt();
		
		// 1.
		if (zahl1 > zahl2 && zahl1 > zahl3)
			System.out.printf("Zahl 1 (%d) ist gr��er als Zahl 2 (%d) und Zahl 3 (%d)\n", zahl1, zahl2, zahl3);
		
		// 2. 
		if (zahl3 > zahl1 || zahl3 > zahl2)
			System.out.printf("Zahl 3 (%d) ist gr��er als Zahl 1 (%d) oder Zahl 2 (%d)\n", zahl3, zahl1, zahl2);
		
		// 3.
		if (zahl1 > zahl2 && zahl1 > zahl3)
			System.out.printf("Zahl 1 ist die gr��te Zahl: %d\n", zahl1);
		else if (zahl2 > zahl1 && zahl2 > zahl3)
			System.out.printf("Zahl 2 ist die gr��te Zahl: %d\n", zahl2);
		else
			System.out.printf("Zahl 3 ist die gr��te Zahl: %d\n", zahl3);
		
		eingabe.close();
	}

}
