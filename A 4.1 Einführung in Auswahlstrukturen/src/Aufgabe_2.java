import java.util.Scanner;

public class Aufgabe_2 {

	public static void main(String[] args) {
		Scanner eingabe = new Scanner(System.in);
		
		double voll = 1.19;
		double ermaessigt = 1.07;
		double nettopreis;
		double bruttopreis;
		char wahl ;

		System.out.print("Bitte geben sie den Nettopreis des Produktes an: ");
		nettopreis = eingabe.nextDouble();

		System.out.print("\nM�chten sie den erm��igten Steuersatz verwenden? \n"
						+"(j)a oder (n)ein: ");
		wahl = eingabe.next().charAt(0);
		
		while (wahl != 'n' && wahl != 'j') {
			
			System.out.print("Bitte geben sie (j)a oder (n)ein ein: ");
			wahl = eingabe.next().charAt(0);
		}

		if (wahl == 'j') {
			System.out.print("\nMit einem Steuersatz von 19% ");
			bruttopreis = nettopreis * ermaessigt;
		}		
		else if (wahl == 'n') {
			System.out.print("\nMit einem Steuersatz von 7% ");
			bruttopreis = nettopreis * voll;
		}
		else {
			System.out.print("Ung�ltige Eingabe");
			bruttopreis = 0;
		}
		
		System.out.printf("betr�gt der Bruttopreis %.2f �", bruttopreis);
		eingabe.close();
	}

}
