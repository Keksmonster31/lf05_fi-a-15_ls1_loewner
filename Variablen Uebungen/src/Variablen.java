/**
 * Variablen.java Ergaenzen Sie nach jedem Kommentar jeweils den Quellcode.
 * 
 * @author Luis L�wner
 * @version 1.0 from 05.10.2021
 */
public class Variablen {
	public static void main(String[] args) {
		/*
		 * 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen. Vereinbaren Sie eine
		 * geeignete Variable
		 */
		long programmdurchlaeufe;

		/*
		 * 2. Weisen Sie dem Zaehler den Wert 25 zu und geben Sie ihn auf dem Bildschirm
		 * aus.
		 */
		programmdurchlaeufe = 25;

		System.out.println("Programmdurchl�ufe: " + programmdurchlaeufe);

		/*
		 * 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt eines Programms
		 * ausgewaehlt werden. Vereinbaren Sie eine geeignete Variable
		 */
		char menuepunkt;

		/*
		 * 4. Weisen Sie dem Buchstaben den Wert 'C' zu und geben Sie ihn auf dem
		 * Bildschirm aus.
		 */
		menuepunkt = 'C';

		System.out.println("Men�punkt: " + menuepunkt);

		/*
		 * 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
		 * notwendig. Vereinbaren Sie eine geeignete Variable
		 */
		double berechnung;

		/*
		 * 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu und geben Sie sie
		 * auf dem Bildschirm aus.
		 */
		berechnung = 299792458;

		System.out.println("Lichtgeschwindigkeit: " + berechnung);

		/*
		 * 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung soll
		 * die Anzahl der Mitglieder erfasst werden. Vereinbaren Sie eine geeignete
		 * Variable und initialisieren sie diese sinnvoll.
		 */

		int mitglieder = 7;

		/* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus. */
		System.out.println("Anzahl der Mitglieder: " + mitglieder);

		/*
		 * 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
		 * Vereinbaren Sie eine geeignete Variable und geben Sie sie auf dem Bildschirm
		 * aus.
		 */
		final double elektrischeElementarladung = 0.0000000000000000001602;

		System.out.println("Elektrische Elementarladung: " + elektrischeElementarladung);

		/*
		 * 10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
		 * Vereinbaren Sie eine geeignete Variable.
		 */
		boolean zahlungErfolgt;

		/*
		 * 11. Die Zahlung ist erfolgt. Weisen Sie der Variable den entsprechenden Wert
		 * zu und geben Sie die Variable auf dem Bildschirm aus.
		 */
		zahlungErfolgt = true;

		System.out.println("Zahlung erfolgt: " + zahlungErfolgt);

	}// main
}// Variablen