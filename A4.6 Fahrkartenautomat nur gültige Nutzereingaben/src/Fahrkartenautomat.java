import java.util.Scanner;

class Fahrkartenautomat {

    public static double fahrkartenbestellungErfassen() {
        int ticket;
    	int anzahlTickets;
        double ticketPreis;
        Scanner tastatur = new Scanner(System.in);
        
        do {
	        System.out.println("W�hlen Sie Ihre gew�nschte Fahrkarte f�r Berlin AB aus:");
	        System.out.printf("%3s\n", "Einzelfahrschein Regeltarif AB [3,00 Euro] (1)");
	        System.out.printf("%3s\n", "4-Fahrten-Karte Einzelfahrausweis AB [9,40 Euro] (2)");
	        System.out.printf("%3s\n\n", "24-Stunden-Karte Regeltarif AB [8,80 Euro] (3)");
	        
	        System.out.print("Ihre Wahl: ");
	        ticket = tastatur.nextInt();
	        if (ticket < 1 || ticket > 3) {
	        	System.out.println("Ung�ltige Eingabe!\n");
	        }
        } while (ticket < 1 || ticket > 3);
        	
        if (ticket == 1) {
        	ticketPreis = 3.00;
        } else if (ticket == 2) {
        	ticketPreis = 9.40;
        } else {
        	ticketPreis = 8.80;
        }
        
        do {
        	System.out.print("Anzahl der Tickets (min: 1 | max: 10): ");
            anzahlTickets = tastatur.nextInt();
            
            if (anzahlTickets < 1 || anzahlTickets > 10) {
            	System.out.printf("\nDie Eingabe f�r die Anzahl der Tickets ist ung�ltig (%d).\nDer Wert muss zwischen 1-10 liegen. \nBitte erneut eingeben.\n\n", anzahlTickets);
            	anzahlTickets = 1;
            } else {
            	break;
            }
        } while (true);
        

        return ticketPreis * anzahlTickets;
    }

    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
        double eingeworfenemuenze;
        double eingezahlterGesamtbetrag = 0.0;
        Scanner tastatur = new Scanner(System.in);

        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.format("Noch zu zahlen: %4.2f Euro %n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
            eingeworfenemuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfenemuenze;
        }
        return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }

    public static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }

    public static void rueckgeldAusgeben(double rueckgabebetrag) {

        if (rueckgabebetrag > 0.0) {
            System.out.format("Der R�ckgabebetrag in H�he von %4.2f Euro %n", rueckgabebetrag);
            System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while (rueckgabebetrag >= 2.0) {// 2 EURO-muenzen
                System.out.println("2 EURO");
                rueckgabebetrag -= 2.0;
            }
            while (rueckgabebetrag >= 1.0) {// 1 EURO-muenzen
                System.out.println("1 EURO");
                rueckgabebetrag -= 1.0;
            }
            while (rueckgabebetrag >= 0.5) // 50 CENT-muenzen
            {
                System.out.println("50 CENT");
                rueckgabebetrag -= 0.5;
            }
            while (rueckgabebetrag >= 0.2) // 20 CENT-muenzen
            {
                System.out.println("20 CENT");
                rueckgabebetrag -= 0.2;
            }
            while (rueckgabebetrag >= 0.1) // 10 CENT-Müzen
            {
                System.out.println("10 CENT");
                rueckgabebetrag -= 0.1;
            }
            while (rueckgabebetrag >= 0.05)// 5 CENT-muenzen
            {
                System.out.println("5 CENT");
                rueckgabebetrag -= 0.05;
            }
        }
    }

    public static void main(String[] args) {
    	
    	while (true) {
	        double zuZahlenderBetrag;
	        double rueckgabebetrag;
	
	        zuZahlenderBetrag = fahrkartenbestellungErfassen();
	        rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
	        fahrkartenAusgeben();
	        rueckgeldAusgeben(rueckgabebetrag);
	
	        System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
	                + "Wir w�nschen Ihnen eine gute Fahrt.");
	        System.out.print("\n\n\n");
    	}
    }
}