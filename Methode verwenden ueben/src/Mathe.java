
public class Mathe {

	public static void main(String[] args) {
		
		System.out.println("Diese Class hat eine einfache Methode namens \"quadrat\" welche recht simpel ist.\n"
				+ "Sie nimmt eine Zahl (Type Double) als Parameter an und gibt das Quadrat der Zahl als R�ckgabewert zur�ck.\n");
		
		double zahl1 = 6;
		
		System.out.printf("Hier ein einfaches Beispiel:\n"
				+ "Das Quadrat von %.2f ist %.2f", zahl1, quadrat(zahl1));
	}

	public static double quadrat(double x) {
		
		return x * x;
	}
	
}
