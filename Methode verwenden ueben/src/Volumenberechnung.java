import java.lang.Math;

public class Volumenberechnung {

	public static void main(String[] args) {
		double wuerfelA = 2;
		System.out.printf("Volumen von einem W�rfel mit a=2\n%f", wuerfel(wuerfelA));
		
		double quaderA = 2;
		double quaderB = 3;
		double quaderC = 4;
		System.out.printf("\n\nVolumen von einem Quader mit a=2, b=3, c=4\n%f", quader(quaderA, quaderB, quaderC));
		
		double pyramideA = 4;
		double pyramideH = 4;
		System.out.printf("\n\nVolumen von einer Pyramide mit a=4, h=4\n%f",pyramide(pyramideA, pyramideH));
		
		double kugelR = 5;
		System.out.printf("\n\nVolumen von einer Kugel mit r=5\n%f", kugel(kugelR));
	}

	public static double wuerfel(double a) {
		return a * a * a;
	}
	
	public static double quader(double a, double b, double c) {
		return a * b * c;
	}
	
	public static double pyramide(double a, double h) {
		return a * a * h / 3.0;		
	}
	
	public static double kugel(double r) {
		return 4.0/3.0 * r * r * r * Math.PI;
	}
}
