
public class mittelwert {

	public static void main(String[] args) {

		double x = 2.0;
		double y = 4.0;
		double m;
		
		m = berechneMittelwert(x,y);
		
		System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
		
	}
	
	public static double berechneMittelwert(double a, double b) {
		return (a + b) / 2;
	}
}

