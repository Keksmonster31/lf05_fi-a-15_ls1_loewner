
public class Schaltungen {

	public static void main(String[] args) {
		
		double r1 = 25;
		double r2 = 10;
		
		System.out.printf("Die Wiederst�nde sind wie folgt:\nr1 = %.2f\nr2 = %.2f\n\n", r1, r2);
		
		System.out.printf("Der Ersatzwiederstand (Rges)der beiden Resistoren in einer Reihenschaltung w�re: %.2f\n", reihenschaltung(r1,r2));
		
		System.out.printf("Der Ersatzwiederstand (Rges) der beiden Resistoren in einer Parallelschaltung w�re: %.2f", parallelschaltung(r1,r2));
	}

	public static double reihenschaltung(double r1, double r2) {
		
		return r1 + r2;
	}
	
	public static double parallelschaltung(double r1, double r2) {
		
		return (r1 * r2) / (r1 + r2);
	}
}
